# Резюме

Приветствую! Я **системный администратор** и **веб-разработчик**.

### Системный администратор

Администрирую системы на платформе:

- Debian
- CentOS
- Fedora

Имею собственные [репозитории](https://copr.fedorainfracloud.org/coprs/marketplace/) для CentOS и Fedora. Собираю и обновляю пакеты.

Устанавливаю и настраиваю различные компоненты системы, например, такие как:

- Apache
- NGINX
- Elasticsearch
- Memcached
- Redis
- MongoDB
- MySQL / MariaDB
- VsFTPD
- Icecast
- ... и многие другие.

### Веб-разработчик

Являюсь автором модулей к различным движкам сайтов и форумов. В разработке использую CMS:

- Drupal ([репозиторий](https://gitlab.com/marketplace-drupal))
- Flarum ([репозиторий](https://gitlab.com/marketplace-flarum))
- MediaWiki ([репозиторий](https://gitlab.com/marketplace-mediawiki))
- NodeBB
- WordPress ([репозиторий](https://gitlab.com/marketplace-wordpress))
- XenForo ([репозиторий](https://gitlab.com/marketplace-xenforo))

Создатель [русской локализации](https://discuss.flarum.org/d/1545) движка [Flarum](https://flarum.org/).

### Pet-проекты

Планирую запустить **UNIX Community** (сообщество *nix-администраторов) и **WEBMASTERS Community** (сообщество для веб-мастеров).

## Навыки

- HTML: **95%**
- CSS: **90%**
- JavaScript: **55%**
- PHP: **40%**
- Git: **60%**
- Shell: **65%**

## Контакты

- Местоположение: [Москва, Россия](https://google.com/maps/d/viewer?mid=1TsLo9d94YFdeEAY351zO_6n6c9U&usp=sharing)
- E-mail: [hello.unix@yandex.ru](mailto:hello.unix@yandex.ru)
