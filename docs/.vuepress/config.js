module.exports = {
    locales: {
        '/': {
            lang: 'ru-RU',
            title: 'Юрий Дунаев',
            description: 'Резюме / Resume / CV'
        },
        '/en/': {
            lang: 'en-US',
            title: 'Yuri Dunaev',
            description: 'Resume / CV'
        }
    },
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Fira+Code:400,700&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://cdn-storage.github.io/styles/vuepress/theme.min.css' }],
        ['link', { rel: 'icon', href: `/logo.png` }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#f3f6f9' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
        ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#f3f6f9' }],
        ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
        ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
    ],
    markdown: {
        lineNumbers: true
    },
    plugins: [
        ['@vuepress/active-header-links', true],
        ['@vuepress/back-to-top', true],
        ['@vuepress/medium-zoom', true],
        ['@vuepress/nprogress', true],
        ['@vuepress/pwa', {
            serviceWorker: true,
            updatePopup: true
        }]
    ],
    themeConfig: {
        editLinks: false,
        locales: {
            '/': {
                label: 'Русский',
                selectText: 'Языки',
                serviceWorker: {
                    updatePopup: {
                        message: "Доступна новая версия контента.",
                        buttonText: "Обновить"
                    }
                },
                nav: [
                    { text: 'Резюме', link: '/' },
                    { text: 'UNIX Docs', link: 'https://unixdocs.gitlab.io/' },
                    { text: 'UNIX AIK', link: 'https://uaik.gitlab.io/' },
                ],
                sidebar: {
                    '/': [
                        {
                            title: 'Обо мне',
                            collapsable: false,
                            children: [
                                '/expertise',
                                '/experience',
                                '/profiles',
                                '/portfolio',
                            ]
                        }
                    ]
                }
            },
            '/en/': {
                label: 'English',
                selectText: 'Languages',
                nav: [
                    { text: 'Resume', link: '/' },
                    { text: 'UNIX Docs', link: 'https://unixdocs.gitlab.io/' },
                    { text: 'UNIX AIK', link: 'https://uaik.gitlab.io/' },
                ],
                sidebar: {
                    '/en/': [
                        {
                            title: 'About',
                            collapsable: false,
                            children: []
                        }
                    ]
                }
            }
        }
    }
};